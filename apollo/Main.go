package main


import (
	"./config"
	"flag"
	"log"
)

func validateNonEmpty(k string, s string) {
	if s == "" {
		log.Fatalf("Param '--%s' can not be empty.", k)
		panic(100)
	}
}

const (
	ConfigDataFile = "config-data-file"
	PortalUrl = "portal-url"
	Token = "token"
)

func main() {
	dataPath := flag.String(ConfigDataFile,"./config-data.txt","config data file path")
	portalUrl := flag.String(PortalUrl,"http://localhost:8080","portal server path")
	token := flag.String(Token,"","your app token")

	flag.Parse()
	log.Printf("config-data-file = %s \n", *dataPath)
	log.Printf("portal-url = %s \n", *portalUrl)
	log.Printf("token = %s \n", *token)
	// Anno function must be called
	/*
	defer func() {
		if err := recover(); err != nil {
			if err == -1 {
				log.Fatalf("Porcess exited with params error : %s .", err)
			}else{
				log.Fatalf("Porcess exited with unknow error: %s .", err)
			}
		}
	}()
	panic(100)
	*/
	// validateNonEmpty(ConfigDataFile, dataPath)
	validateNonEmpty(PortalUrl, *portalUrl)
	validateNonEmpty(Token, *token)
	config.SetToken(*token)

	config.UpdateConfig(*dataPath, *portalUrl)

}
