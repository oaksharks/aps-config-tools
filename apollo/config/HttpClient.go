package config
import (
	"net/http"
	"io/ioutil"
	"bytes"
	"fmt"
	"log"
)

const (
	GET = "GET"
	PUT = "PUT"
	POST = "POST"
    // TOKEN = "0966deafe07b71e431b6cacb6c8f35625b6bb98b"
	CONTENT_TYPE = "application/json"
)

var token string = ""

func SetToken(t string){
	token = t
}

func Post(url string, data string) string {
	return execute(url,POST,data)
}

func Put(url string, data string) string {
	return execute(url,PUT,data)
}
func Get(url string)  string {
	return execute(url, GET, "")
}

func execute(url string, method string, data string) string {
	log.Printf("Rquest to url: %s \n using method: %s \n with data:\n %s \n", url, method, data)
	client := &http.Client{}
	var res *http.Response = nil
	var request,_ = http.NewRequest(method, url, bytes.NewBuffer([]byte(data)))
	request.Header.Set("Content-Type", CONTENT_TYPE)
	request.Header.Set("Authorization", token)
	res, err := client.Do(request)
	defer res.Body.Close()
	if err == nil {
		// correct response
		if res != nil{
			body,_ := ioutil.ReadAll(res.Body)
			resp := string(body)
			log.Printf("Response :%s \n", resp)
			return resp
		}else{
			return ""
		}
	}else{
		fmt.Print(err)
		return "ERROR"
	}
}