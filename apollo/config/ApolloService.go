package config


import (
	"encoding/json"
	"sort"
	"time"
	"log"
	"fmt"
)

type UpdateConfigBody struct {
	Key string `json:"key"`
	Value string `json:"value"`
	Comment string `json:"comment"`
	DataChangeLastModifiedBy string `json:"dataChangeLastModifiedBy"`
}

type ReleaseConfigBody struct {
	ReleaseTitle string `json:"releaseTitle"`
	ReleaseComment string `json:"releaseComment"`
	ReleasedBy string `json:"releasedBy"`
}

// import "fmt"
const (
	UpdateAPI = "%s/openapi/v1/envs/dev/apps/%s/clusters/default/namespaces/%s/items/%s"
	ReleaseAPI = "%s/openapi/v1/envs/dev/apps/%s/clusters/default/namespaces/%s/releases"
)

func UpdateConfig(dataFile string, portalUrl string)  {
	entries := ReadContent(dataFile)
	var i int = 0
	for i =0 ; i < len(entries) ; i ++ {
		e := entries[i]
		updateBody :=  &UpdateConfigBody{ e.key,e.value,"update by tools","apollo"}
		jsonString,err := json.Marshal(updateBody)
		if err == nil{
			updateUrl := fmt.Sprintf(UpdateAPI, portalUrl, e.appId,e.namespace,e.key)
			Put(updateUrl, string(jsonString))
		}
	}
	// fmt.Println("100")
	releaseConfig(entries, portalUrl)
}

func removeDuplicatesAndEmpty(a []string) []string{
	sort.Strings(a)
	var ret []string
	for i:=0; i < len(a); i++{
		if (i > 0 && a[i-1] == a[i]) || len(a[i])==0{
			continue
		}
		ret = append(ret, a[i])
	}
	return ret
}

func releaseConfig(entities []Entry,portalUrl string){
	var releaseUrls = []string{}
	// Summary all namespace and release
	for i:=0 ;i < len(entities) ; i++ {
		e := entities[i]
		releaseUrl := fmt.Sprintf(ReleaseAPI,portalUrl, e.appId, e.namespace)
		releaseUrls = append(releaseUrls, releaseUrl)
	}
	now := time.Now()
	year, month, day := now.Date()
	todayStr := fmt.Sprintf("%d-%d-%d", year, month, day)

	noDuplicatesUrls := removeDuplicatesAndEmpty(releaseUrls)
	for j := 0 ; j < len(noDuplicatesUrls); j++ {
		rcb := &ReleaseConfigBody{todayStr,"release by tools", "apollo"}
		jsonData,err := json.Marshal(rcb)
		if err == nil{
			Post(noDuplicatesUrls[j], string(jsonData))
		}else{
			log.Fatalf("Parse to json error, %s ", err)
		}
	}
	// x := removeDuplicatesAndEmpty(releaseUrls)
}