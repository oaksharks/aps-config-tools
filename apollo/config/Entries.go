package config
/*
Read file content and parse to entry
*/

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
	"log"
)

type Entry struct {
	appId string
	namespace string
	key string
	value string
}

const (
	FieldsNum = 4
)

func ReadContent(dataFile string) []Entry{
	entries := []Entry{}
	inputFile, inputError := os.Open(dataFile)
	if inputError != nil{
		log.Fatalf("Read file error: [%s]", inputError)
	}else{
		defer inputFile.Close()
		inputReader := bufio.NewReader(inputFile)
		for{
			line, readError := inputReader.ReadString('\n')
			if  line[len(line)-1] == '\n'{
				line = line[:len(line) - 1]
			}
			var x = parseLine(line)
			entries = append(entries, x)
			// fmt.Println(entries)
			if readError == io.EOF{
				log.Printf("Reached to tail . \n")
				return entries
			}
		}
	}
	return entries
}

/*
  Parse line by searching # and ignore \#
 */
func parseLine(line string) Entry {
	var positions  [FieldsNum-1]int
	var positionIndex int = 0
	 // l := strings.Count(line, "")-1
    // var i int
    for i := 0; i< len(line); i ++  {
	    v := line[i]
		if v == '#' {
			if i == 0 {
				fmt.Printf("[WARN] the first char is '#' \n")
				positions[positionIndex] = i
				positionIndex = positionIndex + 1
			}else{
				if positionIndex < FieldsNum - 1{
					// check previous char
					pv := line[i - 1]
					if pv == '\\'{
						// do nothing
					}else{
						positions[positionIndex] = i
						positionIndex = positionIndex + 1
					}
				}else{
					fmt.Printf("[WARN] these too mush '#' .\n")
				}
			}
		}
	}
	// check position
	if positionIndex != FieldsNum - 1{
		return Entry{}
	}else{
		return cutsString(positions, line)
	}
	// return positions
	// cuts strings
}

func cutsString(position [FieldsNum - 1]int, line string)  Entry {
	var entry Entry = Entry{
		substr(line,0, position[0]),
		substr(line, position[0] + 1, position[1]),
		substr(line, position[1] + 1, position[2]),
		substr(line, position[2] + 1, len(line)),
	}

	//fmt.Printf("%s-%s-%s-%s", appId, namespace, key, value)
	return entry
}

func substr(str string, start int, end int) string {
	rs := []rune(str)
	length := len(rs)
	if start < 0 || start > length{
		return ""
	}else{
		if end < 0 || end > length {
			return ""
		}else{
			news := string(rs[start:end])
			return strings.Replace(news, "\\#", "#", -1)
		}
	}
}